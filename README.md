# Terragrunt Builder

**How to Use**

```
gcloud builds submit . --config=cloudbuild.yaml
```

This submits a build instruction to Google Cloud Build along with the `Dockerfile`. The `Dockerfile` builds a custom image with Terraform and Terragrunt installed. This image is used later by the CI/CD pipeline to call terragrunt commands.

**Configuring**

Terraform and Terragrunt versions are set within  `cloudbuild.yaml` along with image naming conventions. The image build process is configured in the `Dockerfile`.

**Note**

Ideally this folder and its fles would be hosted in their own repository.
FROM gcr.io/cloud-builders/gcloud as builder

ARG TERRAFORM_VERSION
ARG TERRAGRUNT_VERSION

WORKDIR /builder/terragrunt

RUN apt-get update
RUN apt-get -y install unzip wget curl ca-certificates
RUN curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip > terraform_linux_amd64.zip
RUN unzip terraform_linux_amd64.zip -d /builder/terragrunt

RUN wget -q https://github.com/gruntwork-io/terragrunt/releases/download/v${TERRAGRUNT_VERSION}/terragrunt_linux_amd64

FROM gcr.io/cloud-builders/gcloud

ENV PATH=/builder/terragrunt/:$PATH

WORKDIR /builder/terragrunt

COPY --from=builder /builder/terragrunt/terraform ./
COPY --from=builder /builder/terragrunt/terragrunt_linux_amd64 ./terragrunt

COPY entrypoint.bash /builder/entrypoint.bash

CMD ["ls"]

RUN chmod +x ./terraform
RUN chmod +x ./terragrunt
RUN chmod +x /builder/entrypoint.bash

ENTRYPOINT ["/builder/entrypoint.bash"]